<?php
/**
 * Plugin Name: Odysee Video Gallery
 * Plugin URI:  https://example.com/plugin-name
 * Description: Create odysee video gallery with odysee rss.
 * Version:     1.0.0
 * Author:      Gabriele Rota
 * Author URI:  https://gabrielerota.it/informatica
 * Text Domain: odysee-video-gallery
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

function video_gallery($atts, $content = "") {
$odysee_base_url = "https://odysee.com/";
$odysee_rss = $odysee_base_url."/$/rss/@".$content;
$odysee_embed = $odysee_base_url."/$/embed/@".$content;

$contents = file_get_contents($odysee_rss);
 
// Instantiate XML element
$feed = new SimpleXMLElement($contents);

$items = $feed->channel->item;
$firstItem = $items[0];
$items = array_slice($items, 1, 10);

$firstItemUrl = str_replace($odysee_base_url, $odysee_embed $firstItem->link);

$res.= "<iframe id='odysee-iframe' width='560' height='315' src='$firstItemUrl' allowfullscreen></iframe>"

$res = "<ul>";
     
foreach($items as $entry) {
  //$res.= "<iframe id='odysee-iframe' width='560' height='315' src='https://odysee.com/$/embed/@BorderNights:4/quaglia-gioco-assurdo:c?r=8hTyEFGsQZmjaTEbn9UaicTqshAcWHr4' allowfullscreen></iframe>"
 $res .= "<li><a target='_blank' href='$entry->link' title='$entry->title'>" . $entry->title . "</a></li>";
}
    
$res .= "</ul>";

return $res;
}

add_shortcode( 'odysee_video_gallery', 'video_gallery' );
